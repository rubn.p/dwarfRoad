#ifndef charCreateH
#define charCreateH
#include "../elements/drawable.h"
#include "../gameObjectManager.h"
#include "../gameStateManager.h"
namespace charCreate
{
    void load();
    void initialize();
    void update();
    void draw();
    void free();
    void unload();
}
#endif