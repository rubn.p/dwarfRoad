#ifndef mainMenuH
#define mainMenuH
#include "../elements/drawable.h"
#include "../gameObjectManager.h"
#include "../gameStateManager.h"
namespace mainMenu
{
    void load();
    void initialize();
    void update();
    void draw();
    void free();
    void unload();
}
#endif