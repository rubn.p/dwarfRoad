#include "mainMenu.h"
#include "../gameStateManager.h"
#include "../elements/worldGenerator.h"
extern gameObjectManager* gOM;
extern gameStateManager* gSM;
extern world* gWorld;
namespace mainMenuGlobals
{
    drawable* myDrawable;
}
void mainMenu::load()
{}

void mainMenu::initialize()
{
    //Clean screen
    gSM->cleanConsole();
    //Create the game objects for this state
    //Drawable tittle screen

    mainMenuGlobals::myDrawable = (drawable*) gOM->addGameObject(new drawable("assets/titleScreen.txt"));
    gOM->initialize();
}

void mainMenu::update()
{
    int playerInput = -1;
    //Main logic for the state
    //Print the banner
    mainMenuGlobals::myDrawable->print();
    //Print the text
    std::cout << "A game by Ruben Pena" << std::endl;
    //Print the options
    std::cout << "<0> Continue" << std::endl;
    std::cout << "<1> New game" << std::endl;
    std::cout << "<2> Quit" << std::endl;
    std::cout << "Write a number and press enter" << std::endl;
    while(playerInput != 0 &&playerInput != 1 &&playerInput != 2 )
    {
        std::cout << std::endl << ">> ";
        std::cin >> playerInput;
    }
    if(playerInput == 2)
        gSM->mNextGameState = NULL;
    else if(playerInput == 0)
    {
        gWorld->loadFromFile();
        //gWorld->print();
        gSM->mNextGameState = gSM->mStates_[2];
    }
    else
        gSM->mNextGameState = gSM->mStates_[1];
}

void mainMenu::draw()
{
}

void mainMenu::free()
{}

void mainMenu::unload()
{gOM->clearGameObjects();}