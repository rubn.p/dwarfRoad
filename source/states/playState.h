#ifndef playH
#define playH
#include "../elements/drawable.h"
#include "../gameObjectManager.h"
#include "../gameStateManager.h"
namespace play
{
    void load();
    void initialize();
    void update();
    void draw();
    void free();
    void unload();
}
#endif