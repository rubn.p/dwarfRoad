#ifndef GENERATESTATEH
#define GENERATESTATEH
#include "../elements/worldGenerator.h"
namespace generateState
{
    void load();
    void initialize();
    void update();
    void draw();
    void free();
    void unload();
}
#endif