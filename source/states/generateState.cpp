#include "generateState.h"
#include "../gameObjectManager.h"
#include "../gameStateManager.h"
#include "../elements/worldGenerator.h"
#include <iostream>
extern gameObjectManager* gOM;
extern gameStateManager* gSM;
extern world* gWorld;
void generateState::load()
{}

void generateState::initialize()
{
    gOM->initialize();
}

void generateState::update()
{
    int playerInput = -1;
    gSM->cleanConsole();
    std::cout << "Generating terrain..." << std::endl;
    gWorld->generateWorld();
    gWorld->print();
    std::cout << "<0> Generate another terrain" << std::endl;
    std::cout << "<1> Continue with this terrain and generate biomes" << std::endl;
    while(playerInput != 0 && playerInput != 1)
    {
        playerInput = -1;
        std::cout << ">> ";
        std::cin >> playerInput;
    }
    if(playerInput == 0)
        return;
    playerInput = -1;
    while(playerInput != 1)
    {
        gWorld->generateBiomes();
        gWorld->print();
        std::cout << "<0> Generate another biomes" << std::endl;
        std::cout << "<1> Continue with this biomes and generate cities" << std::endl;
        playerInput = -1;
        std::cout << ">> ";
        std::cin >> playerInput;
        if(playerInput == 1)
            break;
        gWorld->eraseBiomes();
    }
    if(playerInput == 0)
        return;
    playerInput = -1;
    std::string playerString = "0";
    while(true)
    {
        //gSM->cleanConsole();
        if(playerString == "0")
        {
            gWorld->generateCities();
            gWorld->setBase();
        }
        gWorld->print();
        std::cout << "<0> Generate another city layout" << std::endl;
        std::cout << "[City name] Select starting city and go to character creation" << std::endl;
        std::cout << ">> ";
        std::cin >> playerString;
        //Check if the name is correct
        for(int i = 0; i < MAXCITIES; i++)
        {
            if(playerString == gWorld->mCitys[i].mName)
            {
                gWorld->mPlayer.mPosition = gWorld->mCitys[i].mPosition;
                gWorld->setBase();
                gWorld->dumpToFile();
                gSM->mNextGameState = gSM->mStates_[2];
                return;
            }
        }
        if(playerString == "0")
            gWorld->eraseCities();
    }
    gWorld->setBase();
    gWorld->dumpToFile();
    gSM->mNextGameState = gSM->mStates_[2];
}

void generateState::draw()
{}

void generateState::free()
{}

void generateState::unload()
{gOM->clearGameObjects();}
