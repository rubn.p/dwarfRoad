#include "playState.h"
#include "../gameStateManager.h"
#include "../elements/worldGenerator.h"
#include "../elements/player.h"
#include <string>
#include <vector>
extern gameObjectManager* gOM;
extern gameStateManager* gSM;
extern world* gWorld;
//extern player* mPlayer;
namespace playGlobals
{
}
void play::load()
{

}

void play::initialize()
{
    //Clean screen
    gSM->cleanConsole();
    gWorld->setDefault();
    //The player starting position is saved for this scene, so it should be used to initialize the object
    //position temp = mPlayer->mPosition;
    //mPlayer = (player*)gOM->addGameObject(new player("alfred", 0, 0));
    //mPlayer->mPosition = temp;
    gWorld->mGrid[(int)gWorld->mPlayer.mPosition.y][(int)gWorld->mPlayer.mPosition.x] = 'P';
    gWorld->print();
}

void play::update()
{
    //gSM->cleanConsole();
    gWorld->setDefault();
    //Check and undate for citys
    for(city myCity : gWorld->mCitys)
        if(myCity.mPosition.x == gWorld->mPlayer.mPosition.x && myCity.mPosition.y == gWorld->mPlayer.mPosition.y)
            myCity.update();
    bool valid = false;
    std::string mStr;
    std::vector<std::string> mWords;
    std::cout << "Enter an instruction e.g : move south 10" << std::endl;
    std::cout << "Use help to show a list of posible instructions" << std::endl;
    std::cout << ">> ";
    while(!valid)
    {
        getline(std::cin, mStr);
        mWords = gWorld->Tokenize(mStr);
        //Move command
        if(mWords.size() == 3)
        {
            int days = 0;
            if(mWords[0] == "move")
            {
                days = atoi(mWords[2].c_str());
                if(days == 0)
                    break;
                if(mWords[1] == "north")
                {
                    gWorld->mPlayer.mPosition.y -= days;
                    valid = true;
                }
                if(mWords[1] == "south")
                {
                    gWorld->mPlayer.mPosition.y += days;
                    valid = true;
                }
                if(mWords[1] == "west")
                {
                    gWorld->mPlayer.mPosition.x -= days;
                    valid = true;
                }
                if(mWords[1] == "east")
                {
                    gWorld->mPlayer.mPosition.x += days;
                    valid = true;
                }
            }
        }
        //Help command
        if(!mWords.empty() && (mWords[0] == "help" || mWords[0] == "Help"))
        {
            std::cout << "List of commands: " << std::endl;
            std::cout << "help " << std::endl;
            std::cout << "  Displays this help text." << std::endl;
            std::cout << "move <direction> <travel time>" << std::endl;
            std::cout << "  Travel int the given direction for the given time e.g : move south 10 will make you travel south for 10 days." << std::endl;
            std::cout << "  If during the travel there is any event you will travel the distance until that event took place. "<< std::endl;
            std::cout << "map " << std::endl;
            std::cout << "  Displays the map screen. Your position will be marked with a " << "\033[1;31m▮\033[0m"<< std::endl;
            valid = true;
        }
        if(!mWords.empty() && (mWords[0] == "map" || mWords[0] == "Map"))
        {
            gWorld->mGrid[(int)gWorld->mPlayer.mPosition.y][(int)gWorld->mPlayer.mPosition.x] = 'P';
            gWorld->print();
            valid = true;
        }
    }
    
}

void play::draw()
{
}

void play::free()
{}

void play::unload()
{gOM->clearGameObjects();}