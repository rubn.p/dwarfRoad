#include "characterCreation.h"
#include "../gameStateManager.h"
#include "../elements/worldGenerator.h"
extern gameObjectManager* gOM;
extern gameStateManager* gSM;
extern world* gWorld;
namespace charCreateGlobals
{
    drawable* myDrawable;
}
void charCreate::load()
{

}

void charCreate::initialize()
{
    //Clean screen
    gSM->cleanConsole();
    charCreateGlobals::myDrawable = (drawable*) gOM->addGameObject(new drawable("assets/Character.txt"));
    gOM->initialize();
}

void charCreate::update()
{
    int playerInput = -1;
    charCreateGlobals::myDrawable->print();
    std::cout <<"Select your character's class:" << std::endl;
    std::cout <<"<0> Merchant:" << std::endl;
    std::cout <<"<1> Miner:" << std::endl;
    std::cout <<"<2> Warrior:" << std::endl;
    do
    {
        std::cin >> playerInput; 
    } while (playerInput > 2 || playerInput < 0);
    gSM->mNextGameState = gSM->mStates_[3];
}

void charCreate::draw()
{
}

void charCreate::free()
{}

void charCreate::unload()
{gOM->clearGameObjects();}