#include "gameStateManager.h"
#include "elements/worldGenerator.h"
#include "states/mainMenu.h"
#include "states/generateState.h"
#include "states/characterCreation.h"
#include "states/playState.h"
extern gameStateManager * gSM = new gameStateManager;
extern gameObjectManager * gOM = new gameObjectManager;
extern world * gWorld = new world;
namespace mainGlobals
{
    gameState* mainMenu;
    gameState* generateState;
    gameState* characterCreation;
    gameState* playState;
}
void addGameStatesToManager()
{
    //Main menu
    mainGlobals::mainMenu = new gameState("mainMenu");
    mainGlobals::mainMenu->load_ = mainMenu::load;
    mainGlobals::mainMenu->initialize_ = mainMenu::initialize;
    mainGlobals::mainMenu->update_ = mainMenu::update;
    mainGlobals::mainMenu->draw_ = mainMenu::draw;
    mainGlobals::mainMenu->free_ = mainMenu::free;
    mainGlobals::mainMenu->unload_ = mainMenu::unload;
    gSM->addGameState(mainGlobals::mainMenu);
    mainGlobals::generateState = new gameState("generateState");
    mainGlobals::generateState->load_ = generateState::load;
    mainGlobals::generateState->initialize_ = generateState::initialize;
    mainGlobals::generateState->update_ = generateState::update;
    mainGlobals::generateState->draw_ = generateState::draw;
    mainGlobals::generateState->free_ = generateState::free;
    mainGlobals::generateState->unload_ = generateState::unload;
    gSM->addGameState(mainGlobals::generateState);
    //character creation state
    mainGlobals::characterCreation = new gameState("charCreate");
    mainGlobals::characterCreation->load_ = charCreate::load;
    mainGlobals::characterCreation->initialize_ = charCreate::initialize;
    mainGlobals::characterCreation->update_ = charCreate::update;
    mainGlobals::characterCreation->draw_ = charCreate::draw;
    mainGlobals::characterCreation->free_ = charCreate::free;
    mainGlobals::characterCreation->unload_ = charCreate::unload;
    gSM->addGameState(mainGlobals::characterCreation);
    //gameplay state
    mainGlobals::playState = new gameState("play");
    mainGlobals::playState->load_ = play::load;
    mainGlobals::playState->initialize_ = play::initialize;
    mainGlobals::playState->update_ = play::update;
    mainGlobals::playState->draw_ = play::draw;
    mainGlobals::playState->free_ = play::free;
    mainGlobals::playState->unload_ = play::unload;
    gSM->addGameState(mainGlobals::playState);
}

int main(void)
{
    addGameStatesToManager(); 
    gSM->setInitialState(mainGlobals::mainMenu);
    gSM->gameLoop();
    return 0;   
}