#include "gameStateManager.h"
#include <iostream>
//Game state functions
gameState::gameState(const char * name) :mName_(name)
{}

//Game state manager
gameStateManager::gameStateManager()
{}
gameStateManager::~gameStateManager()
{
    free();
    unload();
}
//Add and remove game states
void gameStateManager::addGameState(gameState * gameState)
{
    mStates_.push_back(gameState);
}
void gameStateManager::eraseGameState(gameState * gameState)
{
    for(auto it = mStates_.begin(); it != mStates_.end(); it++)
    {
        if(*it == gameState)
            it = mStates_.erase(it);
        if(it == mStates_.end())
            return;
    }
}
void gameStateManager::setInitialState(gameState *gameState)
{
    mCurrentGameState = gameState;
    mNextGameState = gameState;
}
void gameStateManager::setInitialState(unsigned index)
{
    mCurrentGameState = mStates_[index];
    mNextGameState = mStates_[index];
}
void gameStateManager::gameLoop()
{
    load();
    initialize();
    while (mNextGameState)
    {
        if(mCurrentGameState != mNextGameState)
        {
            free();
            unload();
            mCurrentGameState = mNextGameState;
            load();
            initialize();
        }
        update();
        draw();
    }
}
void gameStateManager::load()
{
    mCurrentGameState->load_();
}
void gameStateManager::initialize()
{
    mCurrentGameState->initialize_();
}
void gameStateManager::update()
{
    mCurrentGameState->update_();
}
void gameStateManager::draw()
{
    mCurrentGameState->draw_();
}
void gameStateManager::free()
{
    mCurrentGameState->free_();
}
void gameStateManager::unload()
{
    mCurrentGameState->unload_();
}
void gameStateManager::cleanConsole()
{
    std::cout << "\x1B[2J\x1B[H";
}