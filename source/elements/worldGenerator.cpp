#include "worldGenerator.h"
#include <random>
#include <time.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
//Rules for world generation
// If the tiles surronding the tiles we are calculating are undefined
// there is a 50% change of it being water or land
//
// Each sourronding water or land tile adds/substracts 20% to the tile
// change to be of the same type
// Water tiles will be ~
// Land tiles will be #
extern world* gWorld;
void world::generateWorld()
{
    //mGrid[i][j]
    double myRandom; //If > 50 land tile, else water tile
    srand(std::time(nullptr));
    //Generate the grid
    //RANDOM GRID
    totalCities = 0;
    for (int i = 0; i < 30; i++)
        for(int j = 0; j < 150; j++)
        {
            myRandom = (double)rand() / (double)RAND_MAX; 
            if(myRandom <= 0.2)
            {
                mGrid[i][j] = '~';
            }
            else
            {
                mGrid[i][j] = '#';
            }
        }
    //TERRAIN ALGORITHM
    for(int i = 0; i < 30; i++)
    {
        //Generate the line
        for(int j = 0; j < 150; j++)
        {
            //Generate random number
            myRandom = (double)rand() / (double)RAND_MAX; 
            //Check left
            if(j != 0 && mGrid[i][j-1] == '~')
                myRandom -= 0.2;
            if(j != 0 && mGrid[i][j-1] == '#')
                myRandom += 1.6;
            //Check right
            if(j != 149 && mGrid[i][j+1] == '~')
                myRandom -= 0.2;
            if(j != 149 && mGrid[i][j+1] == '#')
                myRandom += 1.6;
            //Check up
            if(i != 0 && mGrid[i-1][j] == '~')
                myRandom -= 0.2;
            if(i != 0 && mGrid[i-1][j] == '#')
                myRandom += 1.6;
            //Check down
            if(i != 29 && mGrid[i+1][j] == '~')
                myRandom -= 0.2;
            if(i != 29 && mGrid[i+1][j] == '#')
                myRandom += 1.6;

            //Define and print the tile
            if(myRandom <= 3.8)
            {
                mGrid[i][j] = '~';
                //std::cout << "~";
            }
            else
            {
                mGrid[i][j] = '#';
                //std::cout << "#";
            }
        }
        std::cout << std::endl;
    }
    //Create bioms
}
//Biomes are simbolized on the next way
//Simbol on the array / Biome name / Print simbol
//S                   / Sea        / "\033[1;34m~\033[0m\n"
//F                   / forest     / "\033[1;32m⍋\033[0m\n"
//D                   / Desert     / "\033[1;33m#\033[0m\n"
//M                   / Montains   / "\033[1;37m^\033[0m\n"
void world::generateBiomes()
{
    double myRandom;
    //Generate random bioms
    for(int i = 0; i < 30; i++)
    {
        for(int j = 0; j < 150; j++)
        {
            if(mGrid[i][j] == '#')
            {
                myRandom = (double)rand()/(double)RAND_MAX;
                if(myRandom <= 0.08)
                    mGrid[i][j] = 'F';
                myRandom = (double)rand()/(double)RAND_MAX;
                if(myRandom <= 0.04)
                    mGrid[i][j] = 'D';
                myRandom = (double)rand()/(double)RAND_MAX;
                if(myRandom <= 0.001)
                    mGrid[i][j] = 'M';
            }
        }
    }
    for(int i = 0; i < 30; i++)
    {
        for(int j = 0; j < 150; j ++)
        {
            //Set the ocean tiles to SEA biom
            if(mGrid[i][j] == '~')
            {
                mGrid[i][j] = 'S';
                continue;
            }
            //Create deserts
            myRandom = (double)rand()/(double)RAND_MAX;
            if(j != 0 && mGrid[i][j-1] == 'D')
                myRandom += 0.4;
            if(j != 149 && mGrid[i][j+1] == 'D')
                myRandom += 0.4;
            if(i != 0 && mGrid[i-1][j] == 'D')
                myRandom += 0.4;
            if(i != 29 && mGrid[i+1][j] == 'D')
                myRandom += 0.4;
            if(myRandom > 0.9)
                mGrid[i][j] = 'D';
            //Create forests
            myRandom = (double)rand()/(double)RAND_MAX;
            if(j != 0 && mGrid[i][j-1] == 'F')
                myRandom += 0.5;
            if(j != 149 && mGrid[i][j+1] == 'F')
                myRandom += 0.5;
            if(i != 0 && mGrid[i-1][j] == 'F')
                myRandom += 0.5;
            if(i != 29 && mGrid[i+1][j] == 'F')
                myRandom += 0.5;
            if(myRandom > 0.9)
                mGrid[i][j] = 'F';
            //Create montains
            myRandom = (double)rand()/(double)RAND_MAX;
            if(j != 0 && mGrid[i][j-1] == 'M')
                myRandom += 0.1;
            if(j != 149 && mGrid[i][j+1] == 'M')
                myRandom += 0.1;
            if(i != 0 && mGrid[i-1][j] == 'M')
                myRandom += 0.4;
            if(i != 29 && mGrid[i+1][j] == 'M')
                myRandom += 0.4;
            if(myRandom > 0.9)
                mGrid[i][j] = 'M';
        }
    }

    world::divideBiomes();
}
void world::divideBiomes()
{ 
    for(int i = 0; i < 30; i++)
   {
       for(int j = 0; j < 150; j ++)
       {
           if(mGrid[i][j] == 'F')
           {
               if(world::checkSurroundings('D', i, j))
                    mGrid[i][j] = '#';
           }

           if(mGrid[i][j] == 'M')
           {
               if(world::checkSurroundings('S', i, j))
                    mGrid[i][j] = '#';
           }
       }
   }
}
//Used to reset biomes
void world::eraseBiomes()
{
    for(int i = 0; i < 30; i++)
        for(int j = 0; j < 150; j++)
        {
            switch (mGrid[i][j])
            {
            case 'S':
                mGrid[i][j] = '~';
                break;
            case 'F':
                mGrid[i][j] = '#';
                break;
            case 'D':
                mGrid[i][j] = '#';
                break;
            case 'M':
                mGrid[i][j] = '#';
                break;
            default:
                break;
            }
        }
}
//Generate cities
void world::generateCities()
{
    while(totalDocks < MAXDOCKS)
    {
        double myRandom;
        for(int i = 0; i < 30; i++)
            for(int j = 0; j < 150; j++)
            {
                myRandom = (double)rand() / (double)RAND_MAX;
                if(mGrid[i][j] == '#')
                    if(myRandom <= 0.025 && checkSurroundings('S', i, j)
                    && !checkSurroundings('Q', i, j))
                    {
                        mGrid[i][j] = 'Q';
                        totalDocks++;
                        if(totalDocks >= MAXDOCKS)
                            break;
                    }
            }
    }
    while(totalCities < MAXCITIES)
    {
        double myRandom;
        for(int i = 0; i < 30; i++)
            for(int j = 0; j < 150; j++)
            {
                myRandom = (double)rand() / (double)RAND_MAX;
                if(mGrid[i][j] == '#')
                    if(myRandom <= 0.025 && !checkSurroundings('C', i, j) && checkSurroundings('#', i, j))
                    {
                        mGrid[i][j] = 'C';
                        city myCity(generateCityName().c_str(), j, i);
                        if(checkSurroundings('S', i, j))
                            myCity.type = 'S';
                        else if(checkSurroundings('F', i, j))
                            myCity.type = 'F';
                        else if(checkSurroundings('D', i, j))
                            myCity.type = 'D';
                        else if(checkSurroundings('M', i, j))
                            myCity.type = 'M';
                        //Add the city to the vector
                        mCitys.push_back(myCity);
                        totalCities++;
                        if(totalCities >= MAXCITIES)
                            return;
                    }
            }
    }
}
void world::eraseCities()
{
    mCitys.clear();
    totalCities =0;
    totalDocks = 0;
    for(int i = 0; i < 30; i++)
        for(int j = 0; j < 150; j++)
        {
            if(mGrid[i][j] == 'C')
                mGrid[i][j] = '#';    
            if(mGrid[i][j] == 'Q')
                mGrid[i][j] = '#';    
        }
}
//This is used to draw the world
void world::print()
{
    std::string cityLegend;
    for(int i = 0; i < 30; i++)
    {
        for(int j = 0; j < 150; j++)
        {
            if(mGrid[i][j] == 'S')
                std::cout << "\033[1;34m~\033[0m";
            else if(mGrid[i][j] == 'F')
                std::cout << "\033[1;32m⍋\033[0m";
            else if(mGrid[i][j] == 'D')
                std::cout << "\033[1;33m#\033[0m";
            else if(mGrid[i][j] == 'M')
                std::cout << "\033[1;37m▲\033[0m";
            else if(mBase[i][j] == 'C') //This checks for the base in order to mantain the city legend
            {
                if(mGrid[i][j] == 'P')
                    std::cout << "\033[1;31m▮\033[0m";
                else
                    std::cout << "\033[1;31m▣\033[0m";
                cityLegend += "/ ";
                for(city myCity : mCitys)
                    if(myCity.mPosition.x == j && myCity.mPosition.y == i)
                        cityLegend += myCity.mName;
            }
            else if(mGrid[i][j] == 'P')
                std::cout << "\033[1;31m▮\033[0m";
            else if(mGrid[i][j] == 'Q')
                std::cout << "\033[1;31m⛴\033[0m";
            else
                std::cout << mGrid[i][j];
            continue;
        }
        std::cout << cityLegend;
        cityLegend = "";
        std::cout << std::endl;
    }
}
//checks a positions surroundings for the given element e.g(city)
//returns true if that element exists false if it doesnt
bool world::checkSurroundings(const char& element, int posy, int posx)
{
    //check horizontal
    if(mGrid[posy][posx - 1] == element || mGrid[posy][posx + 1] == element)
        return true;
    //check vertical
    if(mGrid[posy - 1][posx] == element || mGrid[posy + 1][posx] == element)
        return true;
    //check diagonal
    if(mGrid[posy - 1][posx - 1] == element || mGrid[posy + 1][posx - 1] == element || mGrid[posy - 1][posx + 1] == element || mGrid[posy + 1][posx + 1] == element)
        return true;
    return false;
}
void world::dumpToFile()
{
    std::ofstream outfile("./saveFiles/world.txt");
    std::ofstream outNames("./saveFiles/cityNames.txt");
    if(outfile.is_open())
    {
        for(int i = 0; i < 30; i++)
        {
            for(int j = 0; j < 150; j++)
            {
                outfile << mGrid[i][j];
            }
            outfile << std::endl;
        }
    }
    if(outNames.is_open())
    {
        outNames << gWorld->mCitys.size() << std::endl;
        for(auto it : gWorld->mCitys)
        {
            outNames << it.mName << std::endl;
            outNames << it.mPosition.x <<  std::endl << it.mPosition.y << std::endl;
            outNames << it.type << std::endl;
        }
    }
}
void world::loadFromFile()
{
    std::ifstream infile("./saveFiles/world.txt");
    std::ifstream infileName("./saveFiles/cityNames.txt");
    std::string temp;
    int i = 0;
    if(infile.is_open())
    {
        while(!infile.eof())
        {
            if(infile.eof())
                break;
            std::getline(infile, temp);
            for(int j = 0; j < 150; j++)
            {
                mGrid[i][j] = temp[j];
                mBase[i][j] = temp[j];
            }
            i++;
            if(i >= 30)
                break;
        }
        gWorld->mCitys.clear();
        std::getline(infileName, temp);
        int total = std::stoi(temp.c_str());
        i = 0;
        while(!infileName.eof())
        {
            city tempCity;
            std::getline(infileName, tempCity.mName);
            std::getline(infileName, temp);
            std::cout << temp << std::endl;
            tempCity.mPosition.x = std::stoi(temp.c_str());
            std::getline(infileName, temp);
            tempCity.mPosition.y = std::stoi(temp.c_str());
            std::getline(infileName, temp);
            tempCity.type = *temp.c_str();
            gWorld->mCitys.push_back(tempCity);
            if(i++ == total - 1)
                break;
            
        }
            
    }
}

void world::saveCities()
{
    std::ofstream outfile("./saveFiles/cities.txt");
    for(city myCity : mCitys)
        outfile << myCity.mName << std::endl;
}

void world::setBase()
{
    for(int i = 0; i < 30; i++)
    {
        for(int j = 0; j < 150; j++)
        {
            mBase[i][j] = mGrid[i][j];
        }
    }
}
void world::setDefault()
{
    for(int i = 0; i < 30; i++)
    {
        for(int j = 0; j < 150; j++)
        {
            mGrid[i][j] = mBase[i][j];
        }
    }
}
std::string world::generateCityName()
{
    std::string myString;
    while (true)
    {
        int temp = 0;
        float myRand = ((float) rand() / (float) RAND_MAX) * (float)TOTALCITYNAMES;
        std::ifstream infile("assets/cityNames.txt");    
        if(infile.is_open())
            while(temp <= (int)myRand)
            {
                infile >> myString;
                temp++;
            }
        //Avoid two citys with the same name
        for(city myCity : mCitys)
            if(myCity.mName == myString)
                continue;
        return myString;
    }
}
std::vector<std::string> world::Tokenize(const std::string& words)
{
    std::vector<std::string> mWords;
    int start = 0;
    int end = 0;
    //check for a valid sentence
    if (words.find_first_of(' ', end) != std::string::npos)
    {
        while (words.find_first_of(' ', end) != std::string::npos)
        {
            start = words.find_first_not_of(' ', end);
            //end + 1 so that it doent get the same end over and over
            end = words.find_first_of(' ', end + 1);
            //ensure that end is always greater than start
            if (end != -1)
            {
                while (end < start)
                {
                    end = words.find_first_of(' ', end + 1);
                }
            }
            if (start >= 0)
                mWords.push_back(words.substr(start, end - start));
        }
    }
    else
    {
        start = words.find_first_not_of(' ', end);
        end = words.find_first_of(' ', end + 1);
        //if there are words with no spaces push them
        if(start != end)
            mWords.push_back(words);
    }
    return mWords;
}
/*
Esto lo dejo aqui por si haace falta
    char*[]mAllTextures[100];
    char Console[30][150];
    std::cout << Console;
*/