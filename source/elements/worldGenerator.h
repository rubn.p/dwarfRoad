#ifndef WORLDGENERATORH
#define WORLDGENERATORH
#include "../gameObjectManager.h"
#include "player.h"
#include "city.h"
#include "string"
#define MAXCITIES 25
#define TOTALCITYNAMES 50
#define MAXDOCKS 20
class world
{
public:
    //world();
    int totalCities = 0;
    int totalDocks = 0;
    std::vector<city> mCitys;
    city* currentCity;
    //The player
    player mPlayer = player("player", 0, 0);
    //this is the base map, only changed when
    //generating a new one or loading from file
    char mBase[30][150];
    void setBase();
    //restore grid to default values
    void setDefault();
    //this is the array that will be printed to the
    //screen
    char mGrid[30][150];
    void generateWorld();
    void generateBiomes();
    void divideBiomes();
    void eraseBiomes();
    void generateCities();
    void eraseCities();
    void print();
    void dumpToFile();
    void loadFromFile();
    void saveCities();
    void loadCities();
    bool checkSurroundings(const char& element, int posy, int posx);
    std::vector<std::string> Tokenize(const std::string& words);
    std::string generateCityName(); 
};
#endif