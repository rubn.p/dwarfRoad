#ifndef CITYH
#define CITYH
#include "../gameObjectManager.h"
#define CITYLINES 10
class city : public gameObject
{
public:
    city(const char * name = "noname", int x = 0, int y = 0);
    void update();
//Type can be S/M/D/F or Q depending on the surrondings
    char type;
};
#endif