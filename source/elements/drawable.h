#ifndef DRAWABLEH
#define DRAWABLEH
#include <string>
#include <fstream>
#include <iostream>
#include "../gameObjectManager.h"
class drawable : public gameObject 
{
public:
    drawable(const char* directory);
    std::string fileDirectory;
    void print();
};
#endif