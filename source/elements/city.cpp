#include "city.h"
#include <fstream>
#include <random>
#include <iostream>
std::string generateSeaCityLine();
city::city(const char * name, int x, int y)
    : gameObject::gameObject(name, x, y)
{}
void city::update()
{
    switch (type)
    {
    case 'S':
        std::cout << generateSeaCityLine();
        break;
    
    default:
        break;
    }
    std::cout << mName << ".";

    std::cout << std::endl;
}
//This are a serie of functions that will take 
//some random line from the assets to add variety to the messages 
//used
std::string generateSeaCityLine()
{
    float myRandom = (float)CITYLINES * (float)rand() / (float)RAND_MAX;
    std::ifstream infile("assets/seaCityLines.txt");
    std::string myString;
    for(float i = 0; i < myRandom; i++)
        std::getline(infile, myString); 
    return myString;
}