#pragma once
#ifndef gameObjectManagerH
#define gameObjectManagerH
#include <vector>
#include <string>

class position
{
public:
    float x;
    float y;
};
class gameObject
{
public:
    gameObject( const char * name, int x, int y);
    std::string mName;    
    position mPosition;
    void load();
    void initialize();
    void update();
    void draw();
    void free();
    void unload();
};
class gameObjectManager
{
public:
    ~gameObjectManager();
    gameObject* addGameObject(gameObject* gameObject);
    void removeGameObject(gameObject* gameObject);
    void clearGameObjects();
    std::vector<gameObject*> mGameObjects;
    void load();
    void initialize();
    void update();
    void draw();
    void free();
    void unload();
};
#endif