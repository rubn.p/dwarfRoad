#include "gameObjectManager.h"
#include <iostream>
gameObject::gameObject(const char * name, int x = 0, int y = 0)
{
    mPosition.x = x;
    mPosition.y = y;
    if(name)
        mName = name;
    else
        mName = "noName";
}

void gameObject::load()
{}
void gameObject::initialize()
{}
void gameObject::update()
{}
void gameObject::draw()
{}
void gameObject::free()
{}
void gameObject::unload()
{}

gameObjectManager::~gameObjectManager()
{
    clearGameObjects();
}
gameObject* gameObjectManager::addGameObject(gameObject* gameObject)
{
    mGameObjects.push_back(gameObject);
    return gameObject;
}
void gameObjectManager::removeGameObject(gameObject* gameObject)
{
    for(auto it = mGameObjects.begin(); it != mGameObjects.end(); it++)
    {
        if(*it == gameObject)
            it = mGameObjects.erase(it);
        if(it == mGameObjects.end())
            return;
    }
}
void gameObjectManager::clearGameObjects()
{
    mGameObjects.clear();
}
void gameObjectManager::load()
{
    for(gameObject* go : mGameObjects)
        go->load();
}
void gameObjectManager::initialize()
{
    for(gameObject* go : mGameObjects)
        go->initialize();
}
void gameObjectManager::update()
{
    for(gameObject* go : mGameObjects)
        go->update();
}
void gameObjectManager::draw()
{
    for(gameObject* go : mGameObjects)
        go->draw();
}
void gameObjectManager::free()
{
    for(gameObject* go : mGameObjects)
        go->free();
}
void gameObjectManager::unload()
{
    for(gameObject* go : mGameObjects)
        go->unload();
}