#pragma once
#ifndef gameStateManagerH
#define gameStateManagerH
#include <string>
#include <vector>
#include <cstdlib>
class gameState
{
public:
    gameState(const char * name);
    void (*load_)();
    void (*initialize_)();
    void (*update_)();
    void (*draw_)();
    void (*free_)();
    void (*unload_)();
    std::string mName_;
};
class gameStateManager
{
public:
    gameStateManager();
    ~gameStateManager();
    void addGameState(gameState * gameState);
    void eraseGameState(gameState * gameState);
    //void eraseGameState(unsigned index);
    void setInitialState(gameState *gameState);
    void setInitialState(unsigned index);
    void gameLoop();
    void load();
    void initialize();
    void update();
    void draw();
    void free();
    void unload();
    void cleanConsole();
    std::vector<gameState*> mStates_;
    gameState *mCurrentGameState;
    gameState *mNextGameState;
};
#endif